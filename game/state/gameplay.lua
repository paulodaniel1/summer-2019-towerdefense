local Gameplay =
  new "state.base" {
  graphics = nil,
  grid = nil,
  counter = nil,
  tower_specs = nil,
  buy_buttons = nil,
  selected = 1,
  page = 1,
  page_num = 1,
  timer = 0,
  timer_enemy = 0,
  array_de_inimigos = {},
  indice_inimigo = 0,
  derrota = false,
  tempo_atual = 6,
  tempo_passado = 0,
  array_de_torres = {},
  indice_torre = 0,
  timer_tiros = 0,
  array_de_tiros = {},
  indice_tiros = 0
}

function Gameplay:onEnter(graphics)
  self.graphics = graphics
  self:loadTowerSpecs()
  self:createGrid()
  self:createBuyButtons()
  self:createArrowButtons()
  self:createCounter()
  self:changeTowers(1)
end

function Gameplay:loadTowerSpecs()
  self.tower_specs = {}
  for i, specname in require "database":each("towers") do
    self.tower_specs[i] = require("database.towers." .. specname)
  end
  self.page_num = math.ceil(#self.tower_specs / 4)
end

function Gameplay:changeTowers()
  for i = 1, 4 do
    local k = (self.page - 1) * 4 + i
    self.buy_buttons[i].tower_spec = self.tower_specs[k]
  end
  self:selectTower(1)
end

function Gameplay:createGrid()
  self.grid = new "graphics.grid" {}
  self.grid.selected_callback = function(i, j)
    self:buildTower(i, j)
  end
  self.graphics:add("bg", self.grid)
end

function Gameplay:createBuyButtons()
  self.buy_buttons = {}
  local W, H = love.graphics.getDimensions()
  for i = 1, 4 do
    local buy_button
    buy_button = new "graphics.buy_button" {index = i}
    buy_button.callback = function()
      self:selectTower(i)
    end
    self.buy_buttons[i] = buy_button
    self.graphics:add("gui", buy_button)
  end
end

function Gameplay:createArrowButtons()
  local W, H = love.graphics.getDimensions()
  self.graphics:add(
    "gui",
    new "graphics.arrow_button" {
      side = "left",
      position = new(Vec) {240 / 4, 3 * H / 4 + 40},
      callback = function()
        self.page = math.max(1, self.page - 1)
        self:changeTowers()
      end
    }
  )
  self.graphics:add(
    "gui",
    new "graphics.arrow_button" {
      side = "right",
      position = new(Vec) {3 * 240 / 4, 3 * H / 4 + 40},
      callback = function()
        self.page = math.min(self.page_num, self.page + 1)
        self:changeTowers()
      end
    }
  )
end

function Gameplay:createCounter() --OBS: COUNTER É O DINHEIRO
  self.counter = new "graphics.counter" {}
  self.counter:add(100)
  self.graphics:add("gui", self.counter)
end

function Gameplay:buildTower(i, j, spec)
  local spec = self.buy_buttons[self.selected].tower_spec
  if self.counter:getValue() >= spec.cost then
    local tower =
      new "graphics.tower_sprite" {
      spec = spec,
      vida_maxima = spec.vida_maxima,
      vida_atual = spec.vida_maxima,
      grid = self.grid
    }
    self.grid:put(i, j, tower)
    self.indice_torre = self.indice_torre + 1
    self.array_de_torres[self.indice_torre] = {i, j, tower}
    self.counter:subtract(spec.cost)
  end
end

function Gameplay:selectTower(i)
  local button = self.buy_buttons[i]
  if button.tower_spec then
    self.buy_buttons[self.selected].selected = false
    button.selected = true
    self.selected = i
  end
end

function Gameplay:createEnemy(dt)
  self.timer = self.timer + dt
  if self.tempo_passado > 3 then
    self.tempo_atual = self.tempo_atual - 2
    self.tempo_passado = 0
  end
  if self.timer > self.tempo_atual then
    self.tempo_passado = self.tempo_passado + 1
    self.timer = self.timer - self.tempo_atual
    --Cria inimigo aqui
    local i = love.math.random(6)
    local j = 12
    if self.grid:isEmpty(i, j) then
      local spec = require("database.enemies." .. "martian")
      local enemy =
        new "graphics.enemy_sprite" {
        spec = spec,
        grid = self.grid
      }
      self.grid:putEnemy(i, j, enemy)
      self.indice_inimigo = self.indice_inimigo + 1
      self.array_de_inimigos[self.indice_inimigo] = {
        pos_i = i,
        pos_j = j,
        enemy_sprite = enemy,
        movable = false,
        alive = true
      }
    end
  end
end

function Gameplay:moveEnemies(dt)
  self.timer_enemy = self.timer_enemy + dt

  if self.timer_enemy > self.tempo_atual then
    self.timer_enemy = self.timer_enemy - self.tempo_atual

    for i = 1, self.indice_inimigo do
      local inimigo = self.array_de_inimigos[i]
      if not inimigo.alive then
        if inimigo.movable then
          self.grid.map[inimigo.pos_i][inimigo.pos_j] = nil
          inimigo.movable = false
          self.counter = self.counter + inimigo.spec.valor
        end
      end

      if inimigo.movable then
        self.grid.map[inimigo.pos_i][inimigo.pos_j] = nil
        inimigo.pos_j = inimigo.pos_j - 1
        inimigo.enemy_sprite.position =
          self.grid.position + new(Vec) {inimigo.pos_j - 0.5, inimigo.pos_i - 0.5} * self.grid.tilesize
        self.grid.map[inimigo.pos_i][inimigo.pos_j] = inimigo.enemy_sprite
      else
        if inimigo.alive then
          inimigo.movable = true
        end
      end
    end
  end
end

function Gameplay:jogouErradoOtario()
  for i = 1, self.indice_inimigo do
    local inimigo = self.array_de_inimigos[i]
    if (inimigo.pos_j == 1) then
      -- end
      --   for i = 1, 6 do

      --     if not self.grid:isEmpty(i, 1) then
      self.Derrota = true
      local W, H = 1280, 720
      local width, height = 900, 600
      local menu =
        new "graphics.menu" {
        position = new(Vec) {700, 400},
        box = new(Box) {-width / 2, width / 2, -height / 2, height / 2}
      }
      self.graphics:add("gui", menu)
    end
  end
end

function Gameplay:fire(i, j)
  self.timer_tiros = self.timer_tiros + dt

  if self.timer_tiros >= 1 then
    self.timer_tiros = self.timer_tiros - 1

    new_bullet =
      new "bullet" {
      color = {1, 1, 1},
      view = "rectangle",
      params = {"fill", -4, -4, 8, 8},
      power = self.grid.map[i][j].spec.power
    }

    self.grid.putBullet(i + 1, j, new_bullet)
    self.indice_tiros = self.indice_tiros + 1
    self.array_de_tiros[self.indice_tiros] = {
      pos_i = i + 1,
      pos_j = j,
      power = new_bullet.power
    }
  end
end

function Gameplay:onUpdate(dt)
  if not self.Derrota then
    self:createEnemy(dt)
    self:moveEnemies(dt)
    self:jogouErradoOtario()
  end
end

return Gameplay
