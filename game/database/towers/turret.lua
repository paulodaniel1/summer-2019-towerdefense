
return {
  name = "TURRET",
  sprite = 'tesla-turret',
  power = 10,
  cost = 10,
  description = '"10/10 would buy again"',
  vida_maxima = 50,
  firerate = 1,
  shooting_timer = 5
}

