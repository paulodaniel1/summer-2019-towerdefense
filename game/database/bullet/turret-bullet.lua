return {
    name = "TURRET BULLET",
    sprite = nil,
    power = 1,
    hitbox = new(Box) {-4, 4, -4, 4},
    color = {1, 1, 1},
    view = 'rectangle',
    params = {'fill', -4, -4, 8, 8},
}