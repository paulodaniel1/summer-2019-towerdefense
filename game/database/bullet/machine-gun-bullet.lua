return {
    name = "MACHINE GUN BULLET",
    sprite = nil,
    power = 5, 
    hitbox = new(Box) {-4, 4, -4, 4},
    color = {1, 1, 1},
    view = 'rectangle',
    params = {'fill', -4, -4, 8, 8}
}