local Bullet = {}

local Bullet = new(Object) {
    color = nil,
    view = nil,
    params = nil
}

function Bullet:init()
    self.color = self.color or nil
    self.view = self.view or nil
    self.params = self.params or nil
end

return Bullet
