local font_loader = require "graphics.font_loader"

local Menu =
  new "graphics.drawable" {
  box = new(Box) {-10, 200, -30, 300},
  options = nil,
  cursor = 0
}

function Menu:init()
  self.options = {}
  self.bg_color = {.05, .05, .2}
  self.border_color = {1, 1, 1}
  self.text_color = {1, 1, 1}
  self.font = font_loader:get("regular", 25)
end

function Menu:onDraw()
  local g = love.graphics

  --Desenha fundo do menu
  g.setColor(self.bg_color)
  g.rectangle("fill", self.box:getRectangle())
  g.setLineWidth(6)
  g.setColor(self.border_color)
  g.rectangle("line", self.box:getRectangle())

  --Desenha opções

  g.setFont(self.font)
  local step = 300 / (#self.options + 1)
  local y = 200 + step
  local margin = 200 + 20

  local th = 70
  g.print("Num conxegue ganhar né?", -200, 0)
end

return Menu
